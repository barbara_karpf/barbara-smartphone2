﻿
namespace Smartphone.Utility
{
 
  // can cast to a string and use ToString???? for printing
  // can also cast to int???
  // see enum example
  public class Lookups
  {
    public enum ScreenTypes
    { 
      Amoled = 1,
      Lcd = 2, 
      Led = 3   
    }

    public enum MaterialType
    {
      Plastic = 1,
      CarbonFiber = 2,
      Aluminum = 3,
      Glass = 4
    }
  }
}
