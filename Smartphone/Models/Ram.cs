﻿using System;

namespace Smartphone.Models
{
  public class Ram
  {
    // Constructor for a RAM
   public Ram(decimal SizeIn)
    {
      Size = SizeIn;
    }

   // Allow public reads, but only allow writes with Constructor
    public decimal Size { get; private set; }

    // Print method for an instance of RAM
    public void PrintRamInfo()
    {
      Console.WriteLine("\r\n**** My Smartphone Ram ****\r\n");
      Console.WriteLine("  My phone has {0} GB of Ram.\r\n", Size);
      Console.WriteLine();
    }
  }
}
