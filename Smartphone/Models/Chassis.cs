﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Smartphone.Utility;

namespace Smartphone.Models
{
  public class Chassis
  {
    // Constructor for Chassis instance
    public Chassis (List<Lookups.MaterialType> MaterialTypeIn, decimal LengthIn, 
                    decimal WidthIn, decimal HeightIn) 
    {
        MaterialType = MaterialTypeIn;
        Length = LengthIn;
        Width = WidthIn;
        Height = HeightIn;
    }

    // Allow public reads, but only allow writes with Constructor
    public List<Lookups.MaterialType> MaterialType {get; private set;}
    public decimal Length { get; private set; }
    public decimal Width { get; private set; }
    public decimal Height { get; private set; }

    // Print method for an instance of Chassis
    public void PrintChassisInfo() {
      Console.WriteLine("\r\n**** My Smartphone Chassis ****\r\n");
      Console.WriteLine("  Dimensions of {0} in. X {1} in. X {2} in.\r\n", Length, Width, Height);
      Console.Write("  The chassis is made of the following:");
      foreach (var material in MaterialType)
      {
        Console.Write(" {0}", material);
      }
      Console.WriteLine("\r\n");
    }
  }
}
