﻿
namespace Smartphone.Models
{
  public class App
  {
    // Constructor for App instance
    public App(string NameIn)
    {
      Name = NameIn;
    }

    // Allow public reads, but only allow writes with Constructor
    public string Name { get; private set; }
  }
}
