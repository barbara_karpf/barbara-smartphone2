﻿using System;
using Smartphone.Utility;

namespace Smartphone.Models
{
  public class Screen
  {
    // Constructor for a screen
    public Screen(Lookups.ScreenTypes ScreenTypeIn, decimal ScreenSizeIn, int ResolutionWidthIn, 
                  int ResolutionHeightIn, int PixelsPerInchIn, bool IsTouchScreenIn)
    {
      ScreenType = ScreenTypeIn;
      ScreenSize = ScreenSizeIn;
      ResolutionWidth = ResolutionWidthIn;
      ResolutionHeight = ResolutionHeightIn;
      PixelsPerInch = PixelsPerInchIn;
      IsTouchScreen = IsTouchScreenIn;
    }
    
    // Allow public reads, but only allow writes with Constructor
    public Lookups.ScreenTypes ScreenType { get; private set; }
    public decimal ScreenSize { get; private set; }
    public int ResolutionWidth { get; private set; }
    public int ResolutionHeight { get; private set; }
    public int PixelsPerInch { get; private set; }
    public bool IsTouchScreen { get; private set; }

    // Print method for an instance of Screen
    public void PrintScreenInfo()
    {
      Console.WriteLine("\r\n**** My Smartphone Screen Info ****\r\n");
      Console.WriteLine("  My screen type is {0}.\r\n", ScreenType);
      Console.WriteLine("  My screen size is {0} inches diagonally.\r\n", ScreenSize);
      Console.WriteLine("  The resolution of the screen is {0} pixels x {1} pixels.\r\n", ResolutionWidth, ResolutionHeight);
      Console.WriteLine("  It has {0} pixels per inch.\r\n", PixelsPerInch);
      Console.WriteLine("  My phone {0} a touch screen.\r\n", IsTouchScreen ? "has" : "doesn't have");
      Console.WriteLine();
    }

  }
}
