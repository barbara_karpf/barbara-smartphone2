﻿using System;

namespace Smartphone.Models
{
  public class RadioFeatures
  {
    // Constructor for Radio Features instance
    public RadioFeatures(bool HasGpsIn, bool HasWifiIn, bool HasLteIn, bool HasCdmaIn)
    {
      HasGps = HasGpsIn;
      HasWifi = HasWifiIn;
      HasLte = HasLteIn;
      HasCdma = HasCdmaIn;
    }

    // Allow public reads, but only allow writes with Constructor
    public bool HasGps { get; private set; }
    public bool HasWifi { get; private set; }
    public bool HasLte { get; private set; }
    public bool HasCdma { get; private set; }

    // Print method for an instance of RadioFeatures
    public void PrintRadioFeaturesInfo()
    {
      Console.WriteLine("\r\n**** My Smartphone Radio Features ****\r\n");
      Console.WriteLine("  My phone {0} a GPS.\r\n", HasGps ? "has" : "doesn't have");
      Console.WriteLine("  My phone {0} WiFi.\r\n", HasWifi ? "has" : "doesn't have");
      Console.WriteLine("  My phone {0} LTE.\r\n", HasLte ? "has" : "doesn't have");
      Console.WriteLine("  My phone {0} CDMA.\r\n", HasCdma ? "has" : "doesn't have");
      Console.WriteLine();
    }
  
  }
}
