﻿using System;

namespace Smartphone.Models
{
  public class OperatingSys
  {
    // Constructor for OperatingSys instance
    public OperatingSys(string NameIn, string VersionIn)
    {
      Name = NameIn;
      Version = VersionIn;
    }

    // Allow public reads, but only allow writes with Constructor
    public string Name {get; private set;}
    public string Version {get; private set;}

    // Print method for an instance of OperatingSys
    public void PrintOperatingSysInfo()
    {
      Console.WriteLine("\r\n**** My Smartphone Operating System ****\r\n");
      Console.WriteLine("  Name: {0}, Version: {1} \r\n", Name, Version);
      Console.WriteLine();
    }


  }
}
