﻿using System;

namespace Smartphone.Models
{
  public class CentralProcessingUnit
  {
    // Constructor for CentralProcessingUnit instance
    public CentralProcessingUnit(string MakeIn, string ModelIn, decimal ClockSpeedIn)
    {
      Make = MakeIn;
      Model = ModelIn;
      ClockSpeed = ClockSpeedIn;
    }

    // Allow public reads, but only allow writes with Constructor
    public string Make { get; private set; }
    public string Model { get; private set; }
    public decimal ClockSpeed { get; private set; }

    // Print method for an instance of CentralProcessingUnit
    public void PrintCpuInfo() {
      Console.WriteLine("\r\n**** My Smartphone CPU ****\r\n");
      Console.WriteLine("  Make: {0}, Model: {1}, Clock Speed: {2} GHz\r\n", Make, Model, ClockSpeed);
      Console.WriteLine();
    
    }
  }
}
