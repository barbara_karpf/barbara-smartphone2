﻿using System.Collections.Generic;
using System;

namespace Smartphone.Models
{

  public class SmartphoneClass
  {

    public SmartphoneClass(string MakeIn, string ModelIn, decimal WeightIn,
                                bool IsWaterResistantIn, List<App> ApplistIn)
    {
      Make = MakeIn;
      Model = ModelIn;
      Weight = WeightIn;
      IsWaterResistant = IsWaterResistantIn;
      Applist = ApplistIn;
    }

    public string Make { get; private set; }
    public string Model { get; private set; }
    public decimal Weight { get; private set; }
    public bool IsWaterResistant { get; private set; }
    public List<App> Applist { get; private set; }

    public Chassis Chassis { get; set; }

    public CentralProcessingUnit CentralProcessingUnit { get; set; }

    public Ram Ram { get; set; }

    public Camera FrontCamera { get; set; }
    public Camera RearCamera { get; set; }

    public OperatingSys OperatingSystem { get; set; }
    public RadioFeatures RadioFeatures { get; set; }
    public Screen Screen { get; set; }
    public Battery Battery { get; set; }

    public void PrintBasicPhoneSpecs()
    {
      Console.WriteLine();
      Console.WriteLine("**** My Basic Phone Specs ****\r\n");
      Console.WriteLine("Make: {0}, Model: {1}, Weight: {2} oz\r\n", this.Make, this.Model, this.Weight);
      Console.WriteLine("This phone {0} water resistant.\r\n", this.IsWaterResistant ? "is" : "is not");
      Console.WriteLine("This phone has the following apps on it:\r\n");
      foreach (var app in Applist)
      {
        Console.WriteLine("  {0}\r\n", app.Name);
      }
      Console.WriteLine();
    }

    public void PrintALLPhoneSpecs()
    {
      Console.WriteLine();
      Console.WriteLine("**** All Phone Specs ****");
      // don't need this.
      PrintBasicPhoneSpecs();
      Chassis.PrintChassisInfo();
      CentralProcessingUnit.PrintCpuInfo();
      Ram.PrintRamInfo();
      Battery.PrintBatteryInfo();
      Console.WriteLine("**** My Smartphone Front Camera ****\r\n");
      if (FrontCamera != null)
      {
        FrontCamera.PrintCameraInfo(); }
      else
      { Console.WriteLine("  There is no front camera.\r\n\r\n"); }
      Console.WriteLine("**** My Smartphone Rear Camera ****\r\n");
      if (RearCamera != null)
      { RearCamera.PrintCameraInfo(); }
      else
      { Console.WriteLine("  There is no rear camera.\r\n\r\n"); }

      OperatingSystem.PrintOperatingSysInfo();
      RadioFeatures.PrintRadioFeaturesInfo();
      Screen.PrintScreenInfo();
    }

    public void RequestInfoForPhone()
    {
      string consoleInput = "";
      do
      {
        Console.WriteLine();
        Console.WriteLine("What information would you like to know about your phone?\r\n");
        Console.WriteLine("Enter the following to request specific information:\r\n");
        Console.WriteLine("    B for Basic Info (make, model, water resistance, apps)");
        Console.WriteLine("    C for Chassis");
        Console.WriteLine("    CPU for Central Processing Unit");
        Console.WriteLine("    Ram for Ram");
        Console.WriteLine("    Bat for Battery");
        Console.WriteLine("    Front for Front Camera");
        Console.WriteLine("    Rear for Rear Camera");
        Console.WriteLine("    Op for Operating System");
        Console.WriteLine("    R for Radio Features");
        Console.WriteLine("    S for Screen Info");
        Console.WriteLine("    All for All of the Above");
        Console.WriteLine("    Exit to Exit Program\r\n");
        consoleInput = Console.ReadLine();
        // put input into lowercase to make case insensitive
        switch (consoleInput.ToLower())
        {
          case "b":
            PrintBasicPhoneSpecs();
            break;
          case "c":
            Chassis.PrintChassisInfo();
            break;
          case "bat":
            Battery.PrintBatteryInfo();
            break;
          case "cpu":
            CentralProcessingUnit.PrintCpuInfo();
            break;
          case "ram":
            Ram.PrintRamInfo();
            break;
          case "front":
            Console.WriteLine("\r\n**** My Smartphone Front Camera ****\r\n");
            if (FrontCamera != null)
            { FrontCamera.PrintCameraInfo(); }
            else
            { Console.WriteLine("  There is no front camera.\r\n"); }
            break;
          case "rear":
            Console.WriteLine("\r\n**** My Smartphone Rear Camera ****\r\n");
            if (RearCamera != null)
            { RearCamera.PrintCameraInfo(); }
            else
            { Console.WriteLine("  There is no rear camera.\r\n"); }
            break;
          case "op":
            OperatingSystem.PrintOperatingSysInfo();
            break;
          case "r":
            RadioFeatures.PrintRadioFeaturesInfo();
            break;
          case "s":
            Screen.PrintScreenInfo();
            break;
          case "all":
            PrintALLPhoneSpecs();
            break;
        }
        Console.WriteLine();
        Console.WriteLine("Enter Exit to exit or Enter to continue");
        consoleInput = Console.ReadLine();

      } while (consoleInput != "Exit");

    } // end RequestInfoForPhone

  } // end SmartPhoneClass

} // end Namespace
