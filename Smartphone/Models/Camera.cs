﻿using System;

namespace Smartphone.Models
{
  public class Camera
  {
    // Constructor for Camera instance
    public Camera(bool HasFlashIn, int ResolutionWidthIn, int ResolutionHeightIn) {
      HasFlash = HasFlashIn;
      ResolutionWidth = ResolutionWidthIn;
      ResolutionHeight = ResolutionHeightIn;
    }

    // Allow public reads, but only allow writes with Constructor
    public bool HasFlash { get; private set; }
    public int ResolutionWidth { get; private set; }
    public int ResolutionHeight { get; private set; }

    // Print method for an instance of Camera
    public void PrintCameraInfo()
    {
      // Moved comment external to method since front and rear cameras
      //Console.WriteLine("**** My Smartphone Camera ****\r\n");
      Console.WriteLine("  Resolution is {0} pixels x {1} pixels\r\n", ResolutionHeight, ResolutionWidth);
      Console.WriteLine("  The camera {0} a flash.\r\n", HasFlash ? "has" : "doesn't have");
      Console.WriteLine();
    }
  }
}
