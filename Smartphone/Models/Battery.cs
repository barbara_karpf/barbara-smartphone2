﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Smartphone.Models
{
  public class Battery
  {
    // Constructor for Battery instance
    public Battery(decimal KwHin, bool IsRemovableIn, int MaxTalkTimeIn)
    {
      KwH = KwHin;
      IsRemovable = IsRemovableIn;
      MaxTalkTime = MaxTalkTimeIn;
    }

    // Allow public reads, but only allow writes with Constructor
    public decimal KwH { get; private set; }
    public bool IsRemovable { get; private set; }
    public int MaxTalkTime { get; private set; }

    // Print method for an instance of Battery
    public void PrintBatteryInfo()
    {
      Console.WriteLine("\r\n**** My Smartphone Battery ****\r\n");

      Console.WriteLine("  KwH: {0}, MaxTalkTime: {1} hrs\r\n", KwH, MaxTalkTime);
      Console.WriteLine("  The battery {0} removable.\r\n", IsRemovable ? "is" : "is not");
      Console.WriteLine();

    }

  }
}
