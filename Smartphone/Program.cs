﻿using Smartphone.Models;
using Smartphone.Utility;
using System.Collections.Generic;

namespace Smartphone
{
  class Program
  {
    static void Main(string[] args)
    {

      // ----------------------------------------------------------------------------
      // Need to define new instance of a smartphone
      // ----------------------------------------------------------------------------
      
      // Build Applist
      var AppList = new List<App>();
      AppList.Add(new App("Flappy Bird"));
      AppList.Add(new App("Lego World"));
      AppList.Add(new App("Stitcher"));

      // Construct a basic smart phone instance (sc)
      // (Make, Model, Weight, IsWaterResistant, List<App> Applist)
      var sc = new SmartphoneClass("Apple", "iPhone 6", (decimal)4.55, false, AppList);

      // Add chassis instance to smart phone sc instance 
      var MaterialType = new List<Lookups.MaterialType>();
      MaterialType.Add(Lookups.MaterialType.Aluminum);
      MaterialType.Add(Lookups.MaterialType.Plastic);

      //(List of MaterialType, Length(In), Width(In), decimal Height(In)) 
      sc.Chassis = new Chassis(MaterialType, (decimal)5.44, (decimal)2.64, (decimal)0.27);

      // Add CPU instance to smart phone sc instance
      // (Make, Model, Clockspeed(GHz))
      sc.CentralProcessingUnit = new CentralProcessingUnit("Apple", "Dual-Core Cyclone", (decimal)1.4);

      // Add Ram instance to smart phone sc instance
      // (RamSize(GB))
      sc.Ram = new Ram((decimal)1);

      // Add Front Camera instance to smart phone sc instance
      // (HasFlash, ResolutionWidth (px), ResolutionHeight (px))
      // NOTE: do not need a front camera
      sc.FrontCamera = new Camera(true, 3264, 2448);

      // Add Rear Camera instance to smart phone sc instance
      // (HasFlash, ResolutionWidth (px), ResolutionHeight (px))
      // NOTE: do not need a rear camera
      sc.RearCamera = new Camera(false, 1200, 1400);

      // Add Operating System instance to smart phone sc instance
      sc.OperatingSystem = new OperatingSys("iOS", "8");

      // Add Radio Features instance to smart phone sc instance
      //(HasGps, HasWifi, HasLte, HasCdma)
      sc.RadioFeatures = new RadioFeatures(true, true, true, true);

      // Add Screen instance to smart phone sc instance
      // (ScreenType, ScreenSize (diaganol in In), ResolutionWidth (px), 
      //              ResolutionHeight (px), PixelsPerInch, IsTouchScreen?)
      sc.Screen = new Screen(Lookups.ScreenTypes.Lcd, (decimal)4.7, 750, 1334, 326, true);

      // Add Battery instance to smart phone sc instance 
      // (KwH, BatteryRemovable?, MaxTalkTime in hrs)
      sc.Battery = new Battery((decimal).0069, false, 14);

      // ----------------------------------------------------------------------------
      // Print various bits of the smart phone sc instance
      // ----------------------------------------------------------------------------
   
      sc.RequestInfoForPhone();

    } // end Main
  } // end Program
} // end Namespace
